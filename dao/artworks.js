var Artwork = require('../models/artwork'),
	Author= require('../models/author'),
	Users= require('../models/user'),
    nodemailer = require('nodemailer'),
   	express 	= require('express'),
	   app         = express(),
      mongoose = require('mongoose');


var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");

var generator = require('xoauth2').createXOAuth2Generator({
    user: "newsartafrica@gmail.com", // Your gmail address.
    clientId: "270470567184-m27sa3g3ptjing9p7b6im7d70j3u88cs",
    clientSecret: "KJ6VVcg_Ik0VQKUKsW7FTVT4",
    refreshToken: "1/TxtKEFTa9Mg1b2TnvFWZA6BZbQUX6dcO9_3_PPIRTuFIgOrJDtdun6zK6XiATCKT"
});
generator.on('token', function(token){});

var transporter = nodemailer.createTransport(({
    service: 'gmail',
    auth: {
        xoauth2: generator
    }
}));
var adminEmail = 'kriationafrica@gmail.com';


    

var artworks = {

 purchase: function(req, res) {


if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.artworkID)){
var userID = mongoose.Types.ObjectId(req.params.userID),
 idArtwork = mongoose.Types.ObjectId(req.params.artworkID),
 qtyToPurchase = req.params.qty;

   
     Artwork.find({_id:idArtwork, qtyInStock: { $gte: qtyToPurchase } },   function (err, artwork) {
      if (err) {
         res.status(500);
     console.error(err);
      res.json({
            "success": false,
            "message": "Server error"
             });
      } else {
        if (artwork.length == 0) {
           res.status(409);
            res.json({
            "success": false,
            "message": "Not enough quantity in stock"
             });
       
            }
    else {
          Artwork.findByIdAndUpdate(idArtwork, {$inc: { qtyInStock: -qtyToPurchase }, $push:{"purchased": {purchaseByUserID: userID, purchaseQty: qtyToPurchase} } }, {"new": true}, 
      function(err,artwork) {
            if (err) {
            res.status(500);
            res.json({
            "success": false,
            "message": "Server error"
             });
                  } else {

                   var answerPurchase =  artwork.purchased;
                   var last_element = answerPurchase[answerPurchase.length - 1];
                  
               res.json({
                       "purchaseID": last_element._id
                     
             });
             }
        });
    
    }
}
  
  });
 
  }
 else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      } 

},


	// Purchased
	
  purchaseStatus: function(req, res) {
      var param = req.params,
          head = req.headers,
          bodyData = req.body;



if (checkForHexRegExp.test(param.purchaseID) && checkForHexRegExp.test(param.artworkID) && checkForHexRegExp.test(head.userid) ){
var purchaseID = mongoose.Types.ObjectId(param.purchaseID),
 idArtwork = mongoose.Types.ObjectId(param.artworkID),
 userID = mongoose.Types.ObjectId(head.userid),
 qtyToPurchase = param.qty,
 purchaseStatus = param.purchaseStatus;
purchaseStatus =  purchaseStatus.toLowerCase();

    if (purchaseStatus != 'ok') {

        Artwork.find({"purchased._id": purchaseID}, function (err, artwork) {
            if (artwork.length == 0) {
                res.status(400);
                console.error(err);
                res.json({
                    "success": false,
                    "message": "No record in DB"
                });
            } else {


                Artwork.findByIdAndUpdate(idArtwork, {
                        $inc: {qtyInStock: qtyToPurchase},
                        $pull: {"purchased": {_id: purchaseID}}
                    }, {"new": true},
                    function (err, artwork) {
                        if (err) {
                            res.status(500);
                            res.json({
                                "success": false,
                                "message": "Server error"
                            });
                        } else {
                            res.status(409);
                            res.json({
                                "success": false,
                                "message": "Not purchased"
                            });

                        }
                    });

            }

        });

    } else {

        Artwork.findByIdAndUpdate(idArtwork, {$pull: {cart: userID}}, {new: true}, function (err, artwork) {


            Author.findOne({_id: artwork.createdBy}, function (errAuth, artworkAuthor) {


                Users.findOne({_id: userID}, function (errUser, userData) {

                    var shipingInfo = '<p></b>Shipping Fee: </b>' + bodyData.shippingFee + '</p>\
    <p><b>Shipping service: </b>' + bodyData.shippingService + '</p>\
    <p><b>Shipping method: </b>' + bodyData.shippingMethod + '</p>';


                    mailOptionsToUser = {
                        from: 'no-reply <no-reply@newsartafrica.com>', // sender address
                        to: userData.email,
                        subject: 'Purchase information', // Subject line
                        generateTextFromHTML: true,
                        html: '<b>Purchase information!</b>\
    <p>Hello! Dear, <b>' + userData.first + '</b>!</p>\
    <p>Thank that you choose our great app!!!</p>\
    <p>Your purchase details:  </p>\
    <p><b>Art name: </b>' + artwork.name + '</p>\
    <p><b>Cost: </b>' + bodyData.purchaseСost + '</p>' + shipingInfo +
                        '<p><b>Shipping address: </b>Country - ' + userData.country + '</p>\
    <p><b>City - </b>' + userData.city + '</p>\
    <p><b>Street - </b>' + userData.street + '</p>\
    <p>We will contact with to clarify delivery process </p>'
                    };

                    mailOptionsToAdmin = {
                        from: 'no-reply <no-reply@newsartafrica.com>', // sender address
                        to: adminEmail,
                        subject: 'Purchase information', // Subject line
                        generateTextFromHTML: true,
                        html: '<b>Purchase information!</b>\
    <p>User <b>' + userData.first + ' '+ userData.last +'</b></p>\
    <p><b>User email: </b>' + userData.email + '</p>\
    <p><b>Purchase details:  </b></p>\
    <p><b>Art name: </b>' + artwork.name + '</p>\
    <p><b>Art ID: </b>' + idArtwork + '</p>\
    <p><b>Author of Art: </b>' + artworkAuthor.name.first + ' ' +artworkAuthor.name.last+ '</p>\
    <p><b>Author email: </b>' + artworkAuthor.name.first + ' ' +artworkAuthor.name.last+ '</p>\
    <p><b>Cost: </b>' + bodyData.purchaseСost + '</p>' + shipingInfo +
                        '<p><b>Shipping address: </b>Country - ' + userData.country + '</p>\
    <p><b>City - </b>' + userData.city + '</p>\
    <p><b>Street - </b>' + userData.street + '</p>'

                    };

                    mailOptionsToArtist = {
                        from: 'no-reply <no-reply@newsartafrica.com>', // sender address
                        to: artworkAuthor.email,
                        subject: 'Purchase information', // Subject line
                        generateTextFromHTML: true,
                        html: '<b>Purchase information!</b>\
    <p>Hello! Dear, <b>' + artworkAuthor.name.first + '</b></p>\
    <p>User <b>'+  userData.first + ' '+ userData.last +'</b></p>\
    <p>User email: <b>'+ userData.email +' </b></p>\
    <p>Purchase your artwork :</p>\
    <p><b>Art name: </b>' + artwork.name + '</p>\
    <p><b>Art ID: </b>' + idArtwork + '</p>\
    <p>Purchase & Delivery details: </p>\
    <p><b>Cost: </b>' + bodyData.purchaseСost + '</p>' + shipingInfo +
                        '<p><b>Shipping address: </b>Country - ' + userData.country + '</p>\
    <p><b>City - </b>' + userData.city + '</p>\
    <p><b>Street - </b>' + userData.street + '</p>'

                    };







                    transporter.sendMail(mailOptionsToUser, function(err, info) {

                        if(err){
                            return console.log(err);
                            res.status(412);
                            res.json({
                                "success": false,
                                "message": "Fail sending message."
                            });
                        }

                        else{

                            transporter.sendMail(mailOptionsToAdmin, function(err, info) {

                                if(err){
                                    return console.log(err);
                                    res.status(412);
                                    res.json({
                                        "success": false,
                                        "message": "Fail sending message."
                                    });
                                }

                                else{

                                    transporter.sendMail(mailOptionsToArtist, function(err, info) {

                                        if(err){
                                            return console.log(err);
                                            res.status(412);
                                            res.json({
                                                "success": false,
                                                "message": "Fail sending message."
                                            });
                                        }

                                        else{

                                            res.status(200);
                                            res.json({
                                                "success": true,
                                                "message": "purchased"
                                            });

                                        }

                                    });



                                }

                            });



                        }

                    });



                });


            });


        });

    }
 
	
}
 else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of artworkID"
             });
      } 

},

 
   
 getPurchased: function(req, res) {
  if (checkForHexRegExp.test(req.params.userID)){

 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.params.userID);
   
     Artwork.find({'purchased.purchaseByUserID': userID },   function (err, artwork) {
      if (err) {
  res.status(500);
        console.log(err);
           } else {
      if (artwork) {
             artwork.forEach(function (artworkLikes) {

var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}

    });
          res.json(artwork);
        } 
      }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
      }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
  },
	
	
// Cart
  addToCart: function(req, res) {
   if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.artworkID)){

  var userID = mongoose.Types.ObjectId(req.params.userID),
      idArtwork = mongoose.Types.ObjectId(req.params.artworkID);
     Artwork.find({cart: userID, _id:idArtwork},   function (err, artwork) {
      if (err) {
  res.status(500);
        console.log(err);
         res.json({
          "success": false,
            "message": "Server error"
        });
       } else {
      
        if (artwork.length != 0) {
          res.status(409);
          res.json({
            "success": false,
            "message": "Already in cart for this user."
          });
        }
    else {
          Artwork.findByIdAndUpdate(idArtwork, {$push:{cart: userID} }, {"new": true}, function(err,artwork) {
            if (err) {
              return console.error(err);
            } else {
        res.json(artwork);
            }
        });
     
    }

}
  });
   }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
},

  removeFromCart: function(req, res) {
    if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.artworkID)){
 var userID = mongoose.Types.ObjectId(req.params.userID),
      idArtwork = mongoose.Types.ObjectId(req.params.artworkID);
    Artwork.findByIdAndUpdate(idArtwork, {$pull:{cart: userID} },  function(err,artwork) {
            if (err) {
             res.status(404);
          res.json({
            "success": false,
            "message": "Artwork not found."
          });
            } else {
      res.json({
            "success": true,
            "message": "Removed from cart"
          });
            }
        });
      }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
   

},
  
   
 getInCart: function(req, res) {
   if (checkForHexRegExp.test(req.params.userID) ){
 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.params.userID);
   
     Artwork.find({cart: userID },  function (err, artwork) {
      if (err) {
  res.status(404);
        console.log(err);
        res.json({
            "success": false,
            "message": "Artwork not found"
          });
      } else {
        if (artwork.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "Artwork not found"
          });
        }
    else if (artwork) {
           artwork.forEach(function (artworkLikes) {
var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}

    });
          res.json(artwork);
        } 
      }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
      }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
  },

  // Likes
  like: function(req, res) {
     if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.artworkID)){

 var userID = mongoose.Types.ObjectId(req.params.userID),
      idArtwork = mongoose.Types.ObjectId(req.params.artworkID);
     Artwork.find({like: userID, _id:idArtwork},   function (err, artwork) {
      if (err) {
  res.status(500);
  res.json({
            "success": false,
            "message": "Artwork not exist."
          });
        console.log(err);
       } else {
        if (artwork.length != 0) {
          res.status(409);
          res.json({
            "success": false,
            "message": "Already liked by this user."
          });
        }
    else {
          Artwork.findByIdAndUpdate(idArtwork, {$inc: { likes: 1 } , $push:{like: userID} }, {"new": true}, function(err,artwork) {
            if (err) {
              return console.error(err);
            } else {
        res.json(artwork);
            }
        });
     
    }

}
  });
      }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
},

  unlike: function(req, res) {
     if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.artworkID)){
 var userID = mongoose.Types.ObjectId(req.params.userID),
      idArtwork = mongoose.Types.ObjectId(req.params.artworkID);

	  Artwork.find({like: userID, _id:idArtwork},   function (err, artwork) {
      if (err) {
  res.status(500);
        console.log(err);
		 res.json({
            "success": false,
            "message": "Artwork not exist."
          });
       } else {
        if (artwork.length != 0) {
          Artwork.findByIdAndUpdate(idArtwork, {$inc: { likes: -1 } , $pull:{like: userID} }, {"new": true}, function(err,artwork) {
            if (err) {
              return console.error(err);
            } else {
      res.json({
            "success": true,
            "message": "Unliked"
          });
            }
        });
        }
		else {
		 res.status(409);
          res.json({
            "success": false,
            "message": "Already unliked by this user."
          });	
			
		}
	  
	   }
     
	  });
     }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }

},
  
   
 getLikesByUser: function(req, res) {
  if (checkForHexRegExp.test(req.params.userID)){

 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
     userID = mongoose.Types.ObjectId(req.params.userID);
     Artwork.find({like: userID },  function (err, artwork) {
      if (err) {
  res.status(200);
       res.json({
                 });
       } else {
        artwork.forEach(function (artworkLikes) {
var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}

    });
                 res.json(artwork);
            }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
   }
     else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
  },

getArtwork: function(req, res) {
if (checkForHexRegExp.test(req.params.artworkID) && checkForHexRegExp.test(req.headers.userid) ){

var artworkId = mongoose.Types.ObjectId(req.params.artworkID),
    userID = mongoose.Types.ObjectId(req.headers.userid);

     Artwork.findOne({ _id: artworkId }, function  (err, artwork) {
      if (err) {
  res.status(500);
        console.log(err);
         res.json({
          "success": false,
            "message": "Server error"
        });
      } else {
        if (!artwork) {
          res.status(200);
          res.json({
            });
        }


    else if (artwork) {
      var likesArray = artwork.like,
      cartArray = artwork.cart,
      liked = likesArray.indexOf(userID),
      isincart = cartArray.indexOf(userID);
       if (liked >= 0)
       { artwork.isLiked = true;}
   if (isincart>=0)
   	{
   		artwork.inCart = true;
   	}
          res.json(artwork);
        } 
      }
    });
   }
   else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }

  },
  
  getArtworkByDate: function(req, res) {
    if ( checkForHexRegExp.test(req.headers.userid) ){
	var recordpointer = req.params.skeeprecords,
		recordoffset = req.params.offsetrecords,
     userID = mongoose.Types.ObjectId(req.headers.userid);
     Artwork.find({Images: { $exists: true, $ne: [] }}, function (err, artwork) {
      if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (artwork.length == 0) {
          res.status(200);
          res.json({
                });
        }
		else if (artwork) {
artwork.forEach(function (artworkLikes) {
var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}

    });

          res.json(artwork);
        } 
      }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
      }
   else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }
  },
  
  getArtworkByLike: function(req, res) {
     if ( checkForHexRegExp.test(req.headers.userid) ){
  var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.headers.userid);
     Artwork.find({Images: { $exists: true, $ne: [] }}, function (err, artwork) {
      if (err) {
  res.status(500);
        console.log(err);
      } else {
        if (artwork.length == 0) {
          res.status(200);
          res.json({
                    });
        }
    else if (artwork) {
      artwork.forEach(function (artworkLikes) {
var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}

    });
          res.json(artwork);
        } 
      }
    }).skip(recordpointer).sort({likes:-1}).limit(recordoffset);
       }
   else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }
  },
 

 getartworkbyauthor:  function(req, res) {
  
   if (checkForHexRegExp.test(req.params.authorID) && checkForHexRegExp.test(req.headers.userid) ){
   
   var searchAuthor = mongoose.Types.ObjectId(req.params.authorID),
    recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.headers.userid);
    Artwork.find({ createdBy: searchAuthor, Images: { $exists: true, $ne: [] }}, function (err, artwork) {
      if (err) {
   res.status(500);
        console.log(err);
        res.json({
          "success": false,
            "message": "Server error"
        });
      } else {
         artwork.forEach(function (artworkLikes) {
var likesArray = artworkLikes.like,
	cartArray = artworkLikes.cart,
	liked = likesArray.indexOf(userID),
	isincart = cartArray.indexOf(userID);

       if (liked >= 0)
       { artworkLikes.isLiked = true;}
   	if (isincart>=0)
   	{
   		artworkLikes.inCart = true;
   	}
   });
     
          res.json(artwork);
      
      }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
  }
  else {
      res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of authorID"
             });
    
  }
  },


getartworkbycategory: function(req, res) {
  if (req.params.categoryID=='popular-arts')
  {

     if ( checkForHexRegExp.test(req.headers.userid) ) {
         var recordpointer = req.params.skeeprecords,
             recordoffset = req.params.offsetrecords,
             userID = mongoose.Types.ObjectId(req.headers.userid);
         Author.findOne( {isAdmin: true, roles: "Admin"}, function (err, admin) {

         Artwork.find({createdBy: {$ne:  admin._id}, Images: {$exists: true, $ne: []}}, function (err, artwork) {
             if (err) {
                 res.status(500);
                 console.log(err);
             } else {
                 if (artwork.length == 0) {
                     res.status(200);
                     res.json({});
                 }
                 else if (artwork) {
                     artwork.forEach(function (artworkLikes) {
                         var likesArray = artworkLikes.like,
                             cartArray = artworkLikes.cart,
                             liked = likesArray.indexOf(userID),
                             isincart = cartArray.indexOf(userID);

                         if (liked >= 0) {
                             artworkLikes.isLiked = true;
                         }
                         if (isincart >= 0) {
                             artworkLikes.inCart = true;
                         }

                     });
                     res.json(artwork);
                 }
             }
         }).skip(recordpointer).sort({likes: -1}).limit(recordoffset);
     });
       }
   else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }
   

  }
else if (req.params.categoryID=='newest-arts')
{
  
  if ( checkForHexRegExp.test(req.headers.userid) ) {
      var recordpointer = req.params.skeeprecords,
          recordoffset = req.params.offsetrecords,
          userID = mongoose.Types.ObjectId(req.headers.userid);
      Author.findOne( {isAdmin: true, roles: "Admin"}, function (err, admin) {
      Artwork.find({createdBy: {$ne:  admin._id}, Images: {$exists: true, $ne: []}}, function (err, artwork) {
          if (err) {
              res.status(500);
              console.log(err);
          } else {
              if (artwork.length == 0) {
                  res.status(200);
                  res.json({});
              }
              else if (artwork) {
                  artwork.forEach(function (artworkLikes) {
                      var likesArray = artworkLikes.like,
                          cartArray = artworkLikes.cart,
                          liked = likesArray.indexOf(userID),
                          isincart = cartArray.indexOf(userID);

                      if (liked >= 0) {
                          artworkLikes.isLiked = true;
                      }
                      if (isincart >= 0) {
                          artworkLikes.inCart = true;
                      }

                  });

                  res.json(artwork);
              }
          }
      }).skip(recordpointer).sort({_id: -1}).limit(recordoffset);
  });

      }
   else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }



}

else {
	if (checkForHexRegExp.test(req.params.categoryID) && checkForHexRegExp.test(req.headers.userid)) {

        var searchCategory = mongoose.Types.ObjectId(req.params.categoryID),
            recordpointer = req.params.skeeprecords,
            recordoffset = req.params.offsetrecords,
            userID = mongoose.Types.ObjectId(req.headers.userid);

        Author.findOne( {isAdmin: true, roles: "Admin"}, function (err, admin) {
        Artwork.find({createdBy: {$ne:  admin._id}, category: searchCategory, Images: {$exists: true, $ne: []}}, function (err, artwork) {
            if (err) {
                res.status(500);
                console.log(err);
                res.json({
                    "success": false,
                    "message": "Server error"
                });
            } else {

                artwork.forEach(function (artworkLikes) {

                    var likesArray = artworkLikes.like,
                        cartArray = artworkLikes.cart,
                        liked = likesArray.indexOf(userID),
                        isincart = cartArray.indexOf(userID);

                    if (liked >= 0) {
                        artworkLikes.isLiked = true;
                    }
                    if (isincart >= 0) {
                        artworkLikes.inCart = true;
                    }
                });

                res.json(artwork);

            }
        }).skip(recordpointer).sort({_id: -1}).limit(recordoffset);
    });
	}
	else {
		 res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of categoryID"
             });
		
	}

}
	},

   searchArtwork: function(req, res) {
if (checkForHexRegExp.test(req.headers.userid)) {


    var recordpointer = req.params.skeeprecords,
        recordoffset = req.params.offsetrecords
    keyWord = req.params.searchtext,
        userID = mongoose.Types.ObjectId(req.headers.userid);
    ;

    Author.find( {isAdmin: true, roles: "Author"}, function (err, author) {

    Artwork.find({Images: {$exists: true, $ne: []}, createdBy: author._id, $or: [{name: new RegExp(keyWord, "i")}]}, function (err, artwork) {
        if (err) {
            res.status(500);
            console.log(err);
        } else {
            if (artwork.length == 0) {
                res.status(200);
                res.json({});
            }
            else if (artwork) {

                artwork.forEach(function (artworkLikes) {
                    var likesArray = artworkLikes.like,
                        cartArray = artworkLikes.cart,
                        liked = likesArray.indexOf(userID),
                        isincart = cartArray.indexOf(userID);

                    if (liked >= 0) {
                        artworkLikes.isLiked = true;
                    }
                    if (isincart >= 0) {
                        artworkLikes.inCart = true;
                    }
                });

                res.json(artwork);
            }
        }
    }).skip(recordpointer).sort({_id: -1}).limit(recordoffset);

});
      }
  else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
    
  }
}
  
  
 

 
};
 
module.exports = artworks;