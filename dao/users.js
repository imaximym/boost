var User= require('../models/user'),
	jwt    = require('jsonwebtoken'),
	express 	= require('express'),
	app         = express(),
	nodemailer = require('nodemailer'),
	randomstring = require("generate-pincode"),
  mongoose = require('mongoose');
var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");

	var generator = require('xoauth2').createXOAuth2Generator({
   user: "newsartafrica@gmail.com", // Your gmail address.
      clientId: "270470567184-m27sa3g3ptjing9p7b6im7d70j3u88cs",
      clientSecret: "KJ6VVcg_Ik0VQKUKsW7FTVT4",
      refreshToken: "1/TxtKEFTa9Mg1b2TnvFWZA6BZbQUX6dcO9_3_PPIRTuFIgOrJDtdun6zK6XiATCKT"
	});
generator.on('token', function(token){
//    console.log('New token for %s: %s', token.user, token.accessToken);
});

var transporter = nodemailer.createTransport(({
    service: 'gmail',
    auth: {
        xoauth2: generator
    }
}));
var adminEmail = 'kriationafrica@gmail.com';

var	config = require('../config/config'); // get our config file
app.set('superSecret', config.secret); // secret variable

// Crypto parameters 
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'kbye2CNjhdfKmlc256';

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

var users = {
  userauth: function(req, res) {

	// find the user
	User.findOne({
		email: req.body.email
	}, function(err, user) {

		if (err) throw err;

		if (!user) {
			res.status(401);
			res.json({ success: false, message: 'Authentication failed. User not found.' });
		} else if (user) {

			// check if password matches
			if (decrypt(user.password) != req.body.password) {
				res.status(401);
				res.json({ 
			success: false, message: 'Authentication failed. Wrong password.' });
			} else {
				//Object for token genertion
				var userInfo = ({
				email: user.email, 
				password: user.password});

				// if user is found and password is right
				// create a token
				var token = jwt.sign(userInfo, app.get('superSecret'), {
					expiresInMinutes: 525600 // expires in 1 year
				});

				res.json({
 					_id: user._id,
					 first: user.first, 
            last: user.last, 
					country: user.country,
					city: user.city,
          state: user.state,
					postindex: user.postindex,
          street: user.street,
					validemail: user.validemail,
					token: token
				});
			}		

		}

	});
},
 
  
  create: function(req, res) {
	     var body = req.body;
    User.findOne({email: body.email }, function (err,user) {
      if (err) {
        console.log(err);
      } else {
        if (user) {
          res.status(409);
          res.json({
            "success": false,
            "message": "User already exists."
          });
        } 
		
		else {
	
   var activationCode  = randomstring(6);
var mailOptions = {
    from: 'Art News Africa <no-reply@newsartafrica.com>', // sender address
	to: body.email,
    subject: 'Thank you for registration', // Subject line 
    generateTextFromHTML: true,
    html: '<b>Hello! Dear friend!</b>\
    <p>Thank that you choose our great app!!!</p>\
    <p>Please, confirm your e-mail using this code <b>'+activationCode+'</b>\
    (go to app and insert code in your profile screen )</p> ' // html body 
};
	transporter.sendMail(mailOptions, function(err, info){
    if(err){
        return console.log(err);
		 res.status(412);
          res.json({
            "success": false,
            "message": "Fail sending message."
          });
    }
	
	else{
		//Object for token genertion
				var userInfo = ({
				email: body.email,
				password: body.password});

				// if user is found and password is right
				// create a token
				var token = jwt.sign(userInfo, app.get('superSecret'), {
					expiresInMinutes: 525600 // expires in 1 year
				});
				
      var newUser = new User({
      first: body.first, 
      last: body.last, 
			email: body.email,
			password: encrypt(body.password),
			recoveryphrase: activationCode,
			country: body.country, 
      state: body.state,
			city: body.city,
       street: body.street,
			postindex: body.postindex,
			});
		  var returnUserInfo = {
			first: body.first, 
      last: body.last,  
			email: body.email,
			validemail: false,
			country: body.country, 
      recoveryphrase: activationCode,
      state: body.state,
			city: body.city,
       street: body.street,
			postindex: body.postindex,
			token: token
		  };

          newUser.save(function(err,newUser) {
            if (err) {
              return console.error(err);
            } else {
                   	
            	returnUserInfo._id = newUser._id;
				res.json(returnUserInfo );
            }
          });
	}
});		
			
        
        }
      }
    });
	  
  },
  
   update: function(req, res) {

    if (checkForHexRegExp.test(req.params.userID)){
    var body = req.body;
if (typeof body.password!="undefined"){
	body.password = encrypt(req.body.password);
}
    var userRecord = mongoose.Types.ObjectId(req.params.userID);
 
    User.findOne({ _id : userRecord} ,  function (err,user) {
      if (err) {
	res.status(500);
	 res.json({
            "success": false,
            "message": "Server error"
          });
        console.log(err);
      } else {
        if (user) {
          User.findByIdAndUpdate(userRecord, body, {"new": true},  function (err,updateduser) {
            if (err) {
              console.log(err);
			   res.status(409);
          res.json({
            "success": false,
            "message": "Email alredy in use"
          });
            } else {
              res.json(updateduser);
            }
          });
        } else {
          res.status(404);
          res.json({
            "success": false,
            "message": "User not Found"
          });
        }
      }
    });
  }
  else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
 
  },
  
  getOne: function(req, res) {
	   if (checkForHexRegExp.test(req.params.userID)){
   var userSearch = mongoose.Types.ObjectId(req.params.userID);
    User.findOne({ _id : userSearch},  { password:0,  recoveryphrase:0},  function (err, user) {
      if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (user) {
          res.send(user);
        } else {
          res.status(404);
          res.json({
            "success": false,
            "message": "User not Found"
          });
        }
      }
    });
	}
  else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
  },
  
  reset:  function(req, res) {
     var resetmail = req.params.email;
	User.findOne({email: resetmail},  function (err,user) {
      if (err) {
 	res.status(500);
        console.log(err);
      } else {
        if (user) {
			var activationCode  = randomstring(6);
			var mailOptions = {
    from: 'Art News Africa passwordreset <passwordreset@newsartafrica.com>', // sender address
	to: resetmail,
    subject: 'PasswordReset ', // Subject line 
    generateTextFromHTML: true,
    html: '<b>Hello! </b> This is automatic mail.<p> Your reset code is - <b>' + activationCode + '</b><p>If you not request password resseting  - ignore this mail.</p>'// html body 
};
    
   transporter.sendMail(mailOptions, function(err, info){
    if(err){
        return console.log(err);
		 res.status(500);
          res.json({
            "success": false,
            "message": "Fail sending message."
          });
    }  
else{
	 res.status(200);
          res.json({
            "success": true,
            "message": "Recovery code send to e-mail"
          });
	 User.findOneAndUpdate({email: resetmail}, {recoveryphrase:activationCode} ,function (err,updateduser) {
            if (err) {
			console.log(err);}
			});
}	
   });

        } else {
          res.status(404);
          res.json({
            "success": false,
            "message": "Email Not Found"
          });
        }
      }
    });
 
  },
    confirmreset: function(req, res) {
		var resetmail = req.params.email;
	// find the user
	User.findOne({email: req.params.email}, function(err, user) {

		if (err) throw err;

		if (!user) {
		res.status(404);
			res.json({ success: false, message: 'User not found.' });
		} else if (user) {

			// check if password matches
			if (user.recoveryphrase != req.body.recoveryphrase) {
					res.status(401);
				res.json({ success: false, message: 'Authentication failed. Wrong Recovery phrase.' });
			} else {
												
				//Object for token generation
				var userInfo = ({
				email: req.params.email, 
				password: encrypt(req.body.password),
				validemail: true});
				// if user is found and Recovery phrase is right
				// create a token
				var token = jwt.sign(userInfo, app.get('superSecret'), {
					expiresInMinutes: 525600 // expires in 1 year
				});
				 User.findOneAndUpdate({email: req.params.email}, userInfo ,function (err,updateduser) {
            if (err) {
			res.status(500);
			console.log(err);}
			});
				
				res.json({
					success: true,
					 first: user.first, 
            last: user.last, 
					country: user.country,
					city: user.city,
           street: user.street,
          state: user.state,
					postindex: user.postindex,
					validemail: user.validemail,
					email: req.params.email,
					token: token
				});
			}		

		}

	});
},



confirmuseremail: function(req, res) {
	if (checkForHexRegExp.test(req.params.userID)){
   var userRecord = mongoose.Types.ObjectId(req.params.userID);
    var body = req.body.recoveryphrase;
   
 
    User.findOne({ _id : userRecord} , { password:0},  function (err,user) {
      if (err) {
	res.status(500);
        console.log(err);
      } else {
        	if (!user) {
			res.status(401);
			res.json({ success: false, message: 'Authentication failed. User not found.' });
		} else if (user) {

			// check if password matches
			if (user.recoveryphrase != req.body.recoveryphrase) {
				res.status(401);
				res.json({ success: false, message: 'Authentication failed. Wrong confirmation code.' });
			} 
			else {
			
			
          User.findByIdAndUpdate(userRecord, {validemail:true}, {"new": true},  function (err,confirmedusermail) {
            if (err) {
		res.status(500);
              console.log(err);
            } else {
              res.json({
				   "success": true,
					"message": "User email is valid"

			  });
            }
		 });
		}
        } 
      }
    });
 }
  else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
  },

   contactGallery:  function(req, res) {
  if (checkForHexRegExp.test(req.headers.userid) ){
	 var userID = mongoose.Types.ObjectId(req.headers.userid),
	 userMessage = req.body.userMessage;

    User.findOne({_id: userID},  function (err,user) {
      if (err) {
 	res.status(500);
        console.log(err);
      } else {
        if (user) {
			var mailOptions = {
    from: user.email, // sender address 
	to: adminEmail,
    subject: 'Message from User ' + user.first + ' '+ user.last +'(' + user.email + ')', // Subject line 
    generateTextFromHTML: true,
    html: 'User <b>' + user.first + ' '+ user.last + '(' + user.email + ') </b> send message .<p> Message text : <p>' + userMessage + '</p>'// html body 
};
    
   transporter.sendMail(mailOptions, function(err, info){
    if(err){
        return console.log(err);
		 res.status(500);
          res.json({
            "success": false,
            "message": "Fail sending message."
          });
    }  
else{
	 res.status(200);
          res.json({
            "success": true,
            "message": "Message sended"
          });
	
}	
   });

        } else {
          res.status(404);
          res.json({
            "success": false,
            "message": "User not found"
          });
        }
      }
    });
}
 else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }	
 
  }
 
 
};
 
module.exports = users;