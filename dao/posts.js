var Post= require('../models/post'),
	  express 	= require('express'),
	  app         = express(),
    mongoose = require('mongoose'),
    format = require('date-format'),
    checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
	
var posts = {
   
  getNews: function(req, res) {
 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords;
  var pattern = /<(.|\n)*?>|&#?[a-zA-Z0-9]*?;/ig;
   
     Post.find({image: { $gt: {} }},   function (err, post) {
      if (err) {
  res.status(500);
        console.log(err);
           } else {
        if (post.length == 0) {
          res.status(200);
          res.json({
                     });
        }
    else if (post) {
          post.forEach(function (postData) {
              postData.content = postData.content.replace(pattern, '');
              postData.formatDate  = format.asString('yyyy-MM-dd', postData.publishedDate);

                });




          res.json(post);
        } 
      }
    }).skip(recordpointer).sort({publishedDate:-1}).limit(recordoffset);
  },

    searchNews: function(req, res) {
 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    keyWord = req.params.searchtext;

   
   
     Post.find( { $or:[  { title: new RegExp(keyWord, "i")}, { content: new RegExp(keyWord, "i")} ] } ,   function (err, post) {
      if (err) {
  res.status(500);
        console.log(err);
           } else {
        if (post.length == 0) {
          res.status(200);
          res.json({
                     });
        }
    else if (post) {
          res.json(post);
        } 
      }
    }).skip(recordpointer).sort({publishedDate:-1}).limit(recordoffset);
  },

  getOneNewsPost: function(req, res) {
    if (checkForHexRegExp.test(req.params.postID))
{

 var postID = mongoose.Types.ObjectId(req.params.postID),
   imageWidth = 'width="' + req.params.imageWidth + '"',

         patternImage = /alt="[^>]*"/g,
         patternTextFont = /<span style="[^>]*">/g,
         patternTestAlign = /<p style="[^>]*">/g,
                       postContent = '',
                       imageTitle = '',
                       textTitle =  '',
                       publishDate = '',
                       socialIconFacebook = '<div style="float: right; display: inline-block;"><span style="padding-left:5px;  "><a href="https://www.facebook.com/"><img src="https://artnews.s3-eu-west-1.amazonaws.com/facebook.png" width="24" /></a></span>',
                       socialIconTwitter = '<span style="padding-left:5px;"><a href="https://twitter.com/"><img src="https://artnews.s3-eu-west-1.amazonaws.com/twitter.png" width="24" /></a></span>',
                       socialIconInstagram = '<span style="padding-left:5px;"><a href="https://instagram.com/"><img src="https://artnews.s3-eu-west-1.amazonaws.com/instagram.png"  width="24" /></a></span></div>',
                       htmlContent = '',
                       titleStyle = '<p style="text-align: left;"><span style="font-size: 16pt; font-family: helvetica; font-weight: 700; ">',
                       textStyle = '<span style="font-size: 10pt; font-family: helvetica;">',
                       publishDateStyle ='<div style="float: left; display: inline-block; "><span style="font-size: 10pt; font-family: helvetica; color: #999999;">' ;
                     


  
   
     Post.findOne( { _id: postID } ,   function (err, post) {
      if (err) {
  res.status(500);
        console.log(err);
           } 
    else if (post){


                       postContent = post.content;
                       imageTitle = post.image.url;
                       textTitle =  post.title;
                       publishDate = post.publishedDate;
                       publishDate = format.asString('yyyy-MM-dd', publishDate); 
                       
                
            postContent = postContent.replace(patternImage, imageWidth);
         
            postContent = postContent.replace(patternTextFont, '<span style="font-size: 10pt; font-family: helvetica;">');
            postContent = postContent.replace(patternTestAlign, '<p style="text-align: left;">');
                htmlContent = '<p><img src="' + imageTitle + '" alt="" ' + imageWidth + ' /></p>' + titleStyle + textTitle + '</p>' +  publishDateStyle +'Published: ' + publishDate + '</div>' + socialIconFacebook + socialIconTwitter + socialIconInstagram + '<br>' + '<p>' + postContent;



                    post.content = htmlContent;

              res.json(post);
     
        } 
      else
      {
 res.status(200);
         res.json({
            
             });

      }

      
    });
   }
   else
   {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of postID"
             });
   }

  }
 
 
 
};
 
module.exports = posts;