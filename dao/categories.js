var Category= require('../models/category'),
	Artwork = require('../models/artwork'),
	express 	= require('express'),
	app         = express();
	
var categories = {
   
 categorylist: function(req, res) {
	    
    Category.find({image: { $gt: {} }}, function (err,category) {
      if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (category.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "Category list is empty"
          });
        }
		else if (category) {

      Artwork.findOne({Images: { $exists: true, $ne: [] }}, function (err, artworkByDate) {
        //var image = artworkByDate.Images[0];
         var newestCategory =({
        _id: 'newest-arts',
         name: "NEWEST ARTS",
        image: artworkByDate.Images[0]
       });
         category.unshift(newestCategory);
       //  console.log(category);

     Artwork.findOne({Images: { $exists: true, $ne: [] }}, function (err, artworkByLike) {

         var popularCategory =({
        _id: 'popular-arts',
         name: "POPULAR ARTS",
        image: artworkByLike.Images[0]
       });
         category.unshift(popularCategory);
        // console.log(category);
          res.json(category);


     }).sort({likes:-1}).limit(1);

      }).sort({_id:-1}).limit(1);
     
         
        } 
      }
	});
  },

  
   searchCategory: function(req, res) {
 var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords
    keyWord = req.params.searchtext;

   
   
     Category.find( {image: { $gt: {} },  $or:[  { name: new RegExp(keyWord, "i")} ] } ,   function (err, category) {
      if (err) {
  res.status(500);
        console.log(err);
           } else {
        if (category.length == 0) {
          res.status(200);
          res.json({
                     });
        }
    else if (category) {
          res.json(category);
        } 
      }
    }).skip(recordpointer).sort({_id:-1}).limit(recordoffset);
  },
 
 
};
 
module.exports = categories;