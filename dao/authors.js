var Author= require('../models/author'),
Artwork= require('../models/artwork'),
	express 	= require('express'),
	app         = express(),
	nodemailer = require('nodemailer'),
   mongoose = require('mongoose');
  
var reversePopulate = require('mongoose-reverse-populate');
var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
var adminMail = 'kriationafrica@gmail.com';
	
	var generator = require('xoauth2').createXOAuth2Generator({
   user: "newsartafrica@gmail.com", // Your gmail address.
      clientId: "270470567184-m27sa3g3ptjing9p7b6im7d70j3u88cs",
      clientSecret: "KJ6VVcg_Ik0VQKUKsW7FTVT4",
      refreshToken: "1/TxtKEFTa9Mg1b2TnvFWZA6BZbQUX6dcO9_3_PPIRTuFIgOrJDtdun6zK6XiATCKT"
	});
generator.on('token', function(token){
//    console.log('New token for %s: %s', token.user, token.accessToken);
});

var transporter = nodemailer.createTransport(({
    service: 'gmail',
    auth: {
        xoauth2: generator
    }
}));


var authors = {
   
 becomeauthor: function(req, res) {
	     var authormail = req.params.email,
			firstname = req.params.firstname,
      lastname = req.params.lastname;
    Author.findOne({email: authormail }, function (err,author) {
      if (err) {
				res.status(500);
		        console.log(err);
      } else {
        if (author) {
			          res.status(409);
			          res.json({
			            "success": false,
			            "message": "e-mail already in use."
			          });
        } 
		else {
	
var mailOptions = {
    from: 'News Art Africa <no-reply@newsartafrica.com>', // sender address 
	to: authormail,
    subject: 'Thanks for your request', // Subject line 
    generateTextFromHTML: true,
    html: '<p>Hello! Dear, <b>'+ firstname +' !</b></p>\
    <p>Thank you for choosing our great app!!!</p>\
    <p>We appreciate that you want to contribute your artworks throught our application. </p>\
    <p>We will contact with you ASAP.</p><p>With Best regards, </p><p>Gallery Art News Africa </p> ' // html body 
};
var mailOptionsForAdmin = {
    from: 'News Art Africa <no-reply@newsartafrica.com>', // sender address 
	to: adminMail,
    subject: 'Request from user to become artist', // Subject line 
    generateTextFromHTML: true,
    html: '<p>User <b>'+ firstname +'  '+ lastname + '</b></p>\
    <p>Send request from app to become artist. </p>\
    <p>User details: </p> \
    <p>User First name: <b>'+firstname+'</b></p>\
    <p>User Last name: <b>'+lastname+'</b></p>\
    <p>User e-mail address: <b>'+authormail+'</b></p>' // html body 
};
	transporter.sendMail(mailOptions, function(err, info){
    if(err){
	
        return console.log(err);
		 res.status(500);
          res.json({
            "success": false,
            "message": "Fail sending message."
          });
    }
	
	else{
	
     transporter.sendMail(mailOptionsForAdmin, function(err, info){

     	if(err){
	
        return console.log(err);
		 res.status(500);
          res.json({
            "success": false,
            "message": "Fail sending message."
          });
    }
	
	else{
				res.status(200);
				res.json({
				            "success": true,
				            "message": "Request acepted."
          				});
			}
         });

		}
	});		
	       
       		}
      	}
    });
	  
  },
 
     
 listAuthorsByAlphabet: function(req, res) {
     if (checkForHexRegExp.test(req.headers.userid) ){
	var recordpointer = req.params.skeeprecords,
		recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.headers.userid);

		
	Author.find({isAdmin: true, roles: "Author"}).skip(recordpointer).sort({last:-1, first:-1}).limit(recordoffset).exec(function(err, authors) {
 
    var opts = {
        modelArray: authors,
        storeWhere: "works",
        arrayPop: true,
        mongooseModel: Artwork,
        idField: "createdBy"
    }
 



 
    reversePopulate(opts, function(err, popAuthors) {
          if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (popAuthors.length == 0 || popAuthors.works < 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "No authors found"
          });
        }
		else if (popAuthors) {
			var i=0;
 popAuthors.forEach(function (author) {

var followersArray = author.followers;
var artworkArray = author.works;
if (artworkArray.length==0)
{
		popAuthors.splice(i, 1);	
}
     var j=0;
    artworkArray.forEach(function (artworks) {
        var artworkImageAray  = artworks.Images;

        if (artworkImageAray.length==0)
        {
            artworkArray.splice(j, 1);
        }

  var likeArray = artworks.like;
  var cartArray = artworks.cart;
  var liked =    likeArray.indexOf(userID);
       if (liked >= 0)
      { artworks.isLiked = true;}

    var isInCart =    cartArray.indexOf(userID);
       if (isInCart >= 0)
      { artworks.inCart = true;}
        j ++;

    });


 var followed =    followersArray.indexOf(userID);
       if (followed >= 0)
       { author.isFollow = true;}
    i++;
    });


			res.status(200);
          res.json(popAuthors);
        } 
      }
    });
});
 }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }	
	
	
  },
  

  listAuthorsByFollowers: function(req, res) {
     if (checkForHexRegExp.test(req.headers.userid) ){
  var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.headers.userid);
     Author.find({isAdmin: true, roles: "Author"}).skip(recordpointer).sort({followqty:-1}).limit(recordoffset).exec(function(err, authors) {
     var opts = {
        modelArray: authors,
        storeWhere: "works",
        arrayPop: true,
        mongooseModel: Artwork,
        idField: "createdBy"
    }
    
    reversePopulate(opts, function(err, popAuthors) {
          if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (popAuthors.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "No authors found"
          });
        }
		else if (popAuthors) {
			var i=0;
popAuthors.forEach(function (author) {

var followersArray = author.followers;
var artworkArray = author.works;
if (artworkArray.length==0)
{
		popAuthors.splice(i, 1);	
    
}

    var j=0;
    artworkArray.forEach(function (artworks) {
        var artworkImageAray  = artworks.Images;

        if (artworkImageAray.length==0)
        {
            artworkArray.splice(j, 1);
        }


  var likeArray = artworks.like;
  var cartArray = artworks.cart;
  var liked =    likeArray.indexOf(userID);
       if (liked >= 0)
      { artworks.isLiked = true;}

    var isInCart =    cartArray.indexOf(userID);
       if (isInCart >= 0)
      { artworks.inCart = true;}
        j++;

    });




 var followed =    followersArray.indexOf(userID);
       if (followed >= 0)
       { author.isFollow = true;}
   i++;
        });

			res.status(200);
          res.json(popAuthors);
        } 
      }
    });
});	
     }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      } 
	
  },

   listMyFollowAuthors: function(req, res) {
     if (checkForHexRegExp.test(req.headers.userid) ){
  var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords,
    userID = mongoose.Types.ObjectId(req.headers.userid);
     Author.find({isAdmin: true, roles: "Author", followers: userID}).skip(recordpointer).sort({followqty:-1}).limit(recordoffset).exec(function(err, authors) {
     var opts = {
        modelArray: authors,
        storeWhere: "works",
        arrayPop: true,
        mongooseModel: Artwork,
        idField: "createdBy"
    }
    
    reversePopulate(opts, function(err, popAuthors) {
          if (err) {
  res.status(500);
        console.log(err);
      } else {
        if (popAuthors.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "No authors found"
          });
        }
        else if (popAuthors) {
            var i=0;
            popAuthors.forEach(function (author) {

                var followersArray = author.followers;
                var artworkArray = author.works;
                if (artworkArray.length==0)
                {
                    popAuthors.splice(i, 1);

                }

                var j=0;
                artworkArray.forEach(function (artworks) {
                    var artworkImageAray  = artworks.Images;

                    if (artworkImageAray.length==0)
                    {
                        artworkArray.splice(j, 1);
                    }


                    var likeArray = artworks.like;
                    var cartArray = artworks.cart;
                    var liked =    likeArray.indexOf(userID);
                    if (liked >= 0)
                    { artworks.isLiked = true;}

                    var isInCart =    cartArray.indexOf(userID);
                    if (isInCart >= 0)
                    { artworks.inCart = true;}
                    j++;

                });




                var followed =    followersArray.indexOf(userID);
                if (followed >= 0)
                { author.isFollow = true;}
                i++;
            });

            res.status(200);
            res.json(popAuthors);
        }
          }
    });
}); 
     }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      } 
  
  },

  
    getAuthorDetails: function(req, res) {
if (checkForHexRegExp.test(req.params.authorID) ){
var authorSearch = mongoose.Types.ObjectId(req.params.authorID);
  
    Author.find({ _id: authorSearch, isAdmin: true, roles: "Author"}).exec(function(err, authors) {
		 var opts = {
        modelArray: authors,
        storeWhere: "works",
        arrayPop: true,
        mongooseModel: Artwork,
        idField: "createdBy"
    }
	
      reversePopulate(opts, function(err, authorDetail) {
          if (err) {
	res.status(500);
        console.log(err);
      } else {
        if (authorDetail.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "No authors found"
          });
        }
		else if (authorDetail) {
			res.status(200);
          res.json(authorDetail);
        } 
      }
    });
	 });
  }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of authorID"
             });
      }
  },

    follow: function(req, res) {
      if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.authorID) ){
 var userId =  mongoose.Types.ObjectId(req.params.userID),
      idAuthor =  mongoose.Types.ObjectId(req.params.authorID);
     Author.find({followers: userId, _id:idAuthor},   function (err, author) {
      if (err) {
  res.status(200);
       res.json({
           
          });
        console.log(err);
       } else {
        if (author.length != 0) {
          res.status(409);
          res.json({
            "success": false,
            "message": "Already followed by this user."
          });
        }
    else {
          Author.findByIdAndUpdate(idAuthor, {$inc: { followqty: 1 } , $push:{followers: userId} }, {"new": true}, function(err,author) {
            if (err) {
              return console.error(err);
            } else {
               res.status(200);
              res.json(author);
            }
        });
     
    }

}
  });
      }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }
},

  unfollow: function(req, res) {

    if (checkForHexRegExp.test(req.params.userID) && checkForHexRegExp.test(req.params.authorID) ){

 var userId =  mongoose.Types.ObjectId(req.params.userID),
      idAuthor =  mongoose.Types.ObjectId(req.params.authorID);
    Author.find({followers: userId, _id:idAuthor},   function (err, author) {
      if (err) {
  res.status(404);
       res.json({
            "success": false,
            "message": "Author not found"
          });
        console.log(err);
       } else {
        if (author.length != 0) {
          Author.findByIdAndUpdate(idAuthor, {$inc: { followqty: -1 } , $pull:{followers: userId} }, {"new": true}, function(err,author) {
            if (err) {
              return console.error(err);
            } else {
      res.json({
            "success": true,
            "message": "Unfollowed"
          });
            }
        });
        }
    else {
     res.status(409);
          res.json({
            "success": false,
            "message": "Already Unfollowed by this user."
          }); 
      
    }
    
     }
     
    });
    }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      }

},

 searchAuthor: function(req, res) {
     if (checkForHexRegExp.test(req.headers.userid) ){
  var recordpointer = req.params.skeeprecords,
    recordoffset = req.params.offsetrecords, 
    keyWord = req.params.searchtext,
    userID = mongoose.Types.ObjectId(req.headers.userid);
     Author.find({ $or:[  { 'name.first': new RegExp(keyWord, "i")}, { 'name.last': new RegExp(keyWord, "i")} ], isAdmin: true, roles: "Author" }).skip(recordpointer).sort({followers:-1}).limit(recordoffset).exec(function(err, authors) {
     var opts = {
        modelArray: authors,
        storeWhere: "works",
        arrayPop: true,
        mongooseModel: Artwork,
        idField: "createdBy"
    }
    
    reversePopulate(opts, function(err, popAuthors) {
          if (err) {
  res.status(500);
        console.log(err);
      } else {
        if (popAuthors.length == 0) {
          res.status(404);
          res.json({
            "success": false,
            "message": "No authors found"
          });
        }
    else if (popAuthors) {
popAuthors.forEach(function (author) {
var followersArray = author.followers;
 var followed =    followersArray.indexOf(userID);
       if (followed >= 0)
       { author.isFollow = true;}
        });

      res.status(200);
          res.json(popAuthors);
        } 
      }
    });
}); 
     }
    else {
     res.status(409);
         res.json({
            "success": false,
            "message": "Wrong format of userID"
             });
      } 
  
  }


 
};
 
module.exports = authors;