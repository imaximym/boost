var express = require('express');
var router = express.Router(),
	jwt    = require('jsonwebtoken'),
	express 	= require('express'),
	app         = express(),
	user = require('../dao/users.js'),
	artwork = require('../dao/artworks.js'),
	author = require('../dao/authors.js'),
	category = require('../dao/categories.js'),
	news = require('../dao/posts.js'),
	config = require('../config/config'); // get our config file
app.set('superSecret', config.secret); // secret variable

/*
 * Routes without authentication
 */
//router.get('/users', user.getAll);
//router.get('/user/:id', user.getOne);
router.get('/userpasswordreset/:email/', user.reset);
router.post('/userconfirmnewpassword/:email/', user.confirmreset);
router.post('/usercreate/', user.create);
router.post('/userauth/', user.userauth);

//router.put('/user/:id', user.update);
//router.delete('/user/:id', user.delete);

// ---------------------------------------------------------
// route middleware to authenticate and check token
// --------------------------------------------------------- 
 router.use(function(req, res, next) {

	// check header or url parameters or post parameters for token
	var token = req.body.token || req.param('token') || req.headers['x-access-token'];

	// decode token
	if (token) {

		// verifies secret and checks exp
		jwt.verify(token, app.get('superSecret'), function(err, decoded) {			
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });		
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;	
				next();
			}
		});

	} else {

		// if there is no token
		// return an error
		return res.status(403).send({ 
			success: false, 
			message: 'No token provided.'
		});
		
	}
	
});

// ---------------------------------------------------------
// authenticated routes
// ---------------------------------------------------------
 //users
 router.put('/userUpdate/:userID', user.update);
 router.get('/getUserInfo/:userID', user.getOne);
 router.post('/confirmUserEmail/:userID', user.confirmuseremail);
 router.post('/contactGallery/', user.contactGallery);

 //artworks
 router.get('/getArtwork/:artworkID', artwork.getArtwork);
 router.get('/getArtworkByDate/:skeeprecords/:offsetrecords', artwork.getArtworkByDate);
 router.get('/getArtworkByLike/:skeeprecords/:offsetrecords', artwork.getArtworkByLike);
 router.get('/getArtworkByCategory/:categoryID/:skeeprecords/:offsetrecords', artwork.getartworkbycategory);
 router.get('/getArtworkByAuthor/:authorID/:skeeprecords/:offsetrecords', artwork.getartworkbyauthor);
 //authors
  router.post('/becomeAuthor/:email/:firstname/:lastname', author.becomeauthor);
  router.get('/listAuthorsByAlphabet/:skeeprecords/:offsetrecords', author.listAuthorsByAlphabet);
  router.get('/listAuthorsByFollowers/:skeeprecords/:offsetrecords', author.listAuthorsByFollowers);
  router.get('/listMyFollowAuthors/:skeeprecords/:offsetrecords', author.listMyFollowAuthors);
  router.get('/getAuthorDetails/:authorID', author.getAuthorDetails);
  
  //categories
  router.get('/categoryList/', category.categorylist);

  //artworks in cart
   router.post('/addToCart/:userID/:artworkID', artwork.addToCart);
   router.delete('/removeFromCart/:userID/:artworkID', artwork.removeFromCart);
   router.get('/getInCart/:userID/:skeeprecords/:offsetrecords', artwork.getInCart);

	//like artworks
   router.post('/likeArtwork/:userID/:artworkID', artwork.like);
   router.delete('/unlikeArtwork/:userID/:artworkID', artwork.unlike);
   router.get('/getLikesByUser/:userID/:skeeprecords/:offsetrecords', artwork.getLikesByUser);
   
     //purchased artworks
   router.post('/purchaseArtwork/:userID/:artworkID/:qty', artwork.purchase);
   router.post('/purchaseArtworkStatus/:purchaseID/:artworkID/:qty/:purchaseStatus', artwork.purchaseStatus);
   router.get('/getPurchased/:userID/:skeeprecords/:offsetrecords', artwork.getPurchased);

    //follow authors
   router.post('/followAuthor/:userID/:authorID', author.follow);
   router.delete('/unfollowAuthor/:userID/:authorID', author.unfollow);
  
  //Get news
   router.get('/getNews/:skeeprecords/:offsetrecords', news.getNews);
    router.get('/getOneNewsPost/:postID/:imageWidth', news.getOneNewsPost);

  //Search  
    router.get('/searchNews/:searchtext/:skeeprecords/:offsetrecords', news.searchNews);
	router.get('/searchArtwork/:searchtext/:skeeprecords/:offsetrecords', artwork.searchArtwork);
	router.get('/searchAuthor/:searchtext/:skeeprecords/:offsetrecords', author.searchAuthor);
	router.get('/searchCategoty/:searchtext/:skeeprecords/:offsetrecords', category.searchCategory);
   
 
module.exports = router;


