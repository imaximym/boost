var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('post', new Schema({ 

	title: {type: String},
	content: {type: String},
	publishedDate: {type: Date},
	formatDate : {type: String},
	image: {
			url: {type: String}

	}


}));