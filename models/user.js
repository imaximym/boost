var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('appuser', new Schema({ 
			
	first:{type: String},
	last: { type: String },
	email:	  { type: String, required: true, unique: true },
	password: { type: String, required: true },
	recoveryphrase: { type: String, default: ''},
	validemail:  { type: Boolean, default: false}, 
	country: { type: String},
	street: { type: String},
	city: { type: String }, 
	state: { type: String }, 
	postindex: { type: String }
	

}));