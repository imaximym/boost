
var mongoose = require('mongoose'),
 Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('art', new Schema({

	createdBy: {type: Schema.Types.ObjectId, ref: 'user'},
	name: { type: String},
	Images: [],
	likes: { type: Number },
	cart: [{ type: Schema.Types.ObjectId }],
	like: [{ type: Schema.Types.ObjectId }],
	category: {type: Schema.Types.ObjectId, ref: 'categorie'},
	purchased: [{
			
			purchaseByUserID:{type: Schema.Types.ObjectId},
			purchaseQty: { type: Number },
			purchaseDate: { type : Date, default: Date.now  }
	}],
	qtyInStock: { type: Number },
	reserved: [{
		
			reservedByUserID:{type: Schema.Types.ObjectId},
			reservedQty: { type: Number },
			reservedDate: { type : Date, default: Date.now  }
	}],
	isLiked: { type: Boolean, default: false},
	inCart: { type: Boolean, default: false}
	
	
	
}));
