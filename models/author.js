var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model
module.exports = mongoose.model('user', new Schema({ 
	_id: {type: Schema.Types.ObjectId},
	name: {
		last: {type : String},
		first: {type : String}
	},
	email: { type: String},
	followers: [{ type: Schema.Types.ObjectId }],
	followqty: { type: Number, default: 0 },
	isAdmin: {type: Boolean, default: false},
	works: [],
	isFollow: { type: Boolean, default: false}
		
}));